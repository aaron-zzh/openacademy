# -*- coding: utf-8 -*-
from odoo import models, fields, api


# 向导通过动态表单描述与用户(或对话框)的交互会话。向导也是一个模型，扩展了类TransientModel而不是model。
class Wizard(models.TransientModel):
    _name = 'openacademy.wizard'
    _description = "Wizard: Quick Registration of Attendees to Sessions"

    def _default_sessions(self):
        return self.env['openacademy.session'].browse(self._context.get('active_ids'))

    session_ids = fields.Many2many('openacademy.session',
        string="Session", required=True, default=_default_sessions)
    attendee_ids = fields.Many2many('res.partner', string="Attendees")
    
    # Register attendees to multiple sessions
    def subscribe(self):
        for session in self.session_ids:
            session.attendee_ids |= self.attendee_ids
        return {}
