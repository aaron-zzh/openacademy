# -*- coding: utf-8 -*-
from odoo import fields, models


# 模型继承，未修改 _name，兼容原模型视图，数据存储在同一个表中
class Partner(models.Model):
    _inherit = 'res.partner'

    # 添加一个instructor字段到 res.partner 模型, 默认值 False
    instructor = fields.Boolean("Instructor", default=False)

    session_ids = fields.Many2many('openacademy.session',
        string="Attended Sessions", readonly=True)
