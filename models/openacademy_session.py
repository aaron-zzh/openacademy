# -*- coding: utf-8 -*-
from datetime import timedelta
from odoo import models, fields, api, exceptions


# 定义授课/排课模型
class Session(models.Model):
    _name = 'openacademy.session'
    _description = "OpenAcademy Sessions"

    name = fields.Char(required=True)
    # 通过 default 设置默认值
    start_date = fields.Date(default=fields.Date.today)
    duration = fields.Float(digits=(6, 2), help="Duration in days")
    end_date = fields.Date(string="End Date", store=True,
        compute='_get_end_date', inverse='_set_end_date')
    attendees_count = fields.Integer(
        string="Attendees count", compute='_get_attendees_count', store=True)
    seats = fields.Integer(string="Number of seats")
    # Odoo有内置规则记录active=False时不可见。
    active = fields.Boolean(default=True)
    color = fields.Integer()

    # 计算属性字段，出席占比。
    taken_seats = fields.Float(string="Taken seats", compute='_taken_seats')
    # m2o 关联字段，讲师，当为选择教师时，instructor字段值为True 或类别匹配 Teacher 的 partner 是可见的。
    instructor_id = fields.Many2one('res.partner', string="Instructor",
        domain=['|', ('instructor', '=', True), ('category_id.name', 'ilike', "Teacher")])
    # 课程字段，ondelete='cascade' 删除课程记录时删除关联的授课记录。
    course_id = fields.Many2one('openacademy.course',
        ondelete='cascade', string="Course", required=True)
    # m2m 关联字段，每个授课有多个出席人员
    attendee_ids = fields.Many2many('res.partner', string="Attendees")

    state = fields.Selection([
        ('draft', "Draft"),
        ('confirmed', "Confirmed"),
        ('done', "Done"),
    ], default='draft')

    @api.multi
    def action_draft(self):
        self.state = 'draft'

    @api.multi
    def action_confirm(self):
        self.state = 'confirmed'

    @api.multi
    def action_done(self):
        self.state = 'done'

    # 计算字段的值通常取决于计算记录上的其他字段的值。使用depends()装饰器指定依赖关系。
    @api.depends('seats', 'attendee_ids')
    def _taken_seats(self):
        for r in self:
            if not r.seats:
                r.taken_seats = 0.0
            else:
                r.taken_seats = 100.0 * len(r.attendee_ids) / r.seats

    # onchange 机制为客户端接口提供了一种方法，在用户在字段中填写值时更新表单，而不将任何内容保存到数据库中。
    @api.onchange('seats', 'attendee_ids')
    def _verify_valid_seats(self):
        if self.seats < 0:
            return {
                'warning': {
                    'title': "Incorrect 'seats' value",
                    'message': "The number of available seats may not be negative",
                },
            }
        if self.seats < len(self.attendee_ids):
            return {
                'warning': {
                    'title': "Too many attendees",
                    'message': "Increase seats or remove excess attendees",
                },
            }

    # 模型约束，通过 python 约束 课程讲师不能是出席人员
    @api.constrains('instructor_id', 'attendee_ids')
    def _check_instructor_not_in_attendees(self):
        for r in self:
            if r.instructor_id and r.instructor_id in r.attendee_ids:
                raise exceptions.ValidationError("A session's instructor can't be an attendee")

    @api.depends('start_date', 'duration')
    def _get_end_date(self):
        for r in self:
            if not (r.start_date and r.duration):
                r.end_date = r.start_date
                continue

            # Add duration to start_date, but: Monday + 5 days = Saturday, so
            # subtract one second to get on Friday instead
            duration = timedelta(days=r.duration, seconds=-1)
            r.end_date = r.start_date + duration

    def _set_end_date(self):
        for r in self:
            if not (r.start_date and r.end_date):
                continue

            # Compute the difference between dates, but: Friday - Monday = 4 days,
            # so add one day to get 5 days instead
            r.duration = (r.end_date - r.start_date).days + 1

    @api.depends('attendee_ids')
    def _get_attendees_count(self):
        for r in self:
            r.attendees_count = len(r.attendee_ids)






