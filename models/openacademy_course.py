# -*- coding: utf-8 -*-
from odoo import models, fields, api, _


# 定义 课程 模型
class Course(models.Model):
    # 特殊字段 Odoo 要求在所有模型上至少有一个 name 字段，用于各种显示和搜索行为。
    _name = 'openacademy.course'
    _description = "OpenAcademy Courses"

    # 标题字段，字段属性：名称为 title，为必须字段
    name = fields.Char(string="Title", required=True)
    description = fields.Text()
    # 字段有两大类:直接存储在模型表中的原子值简单字段和链接记录(相同模型或不同模型)的关系字段。
    # 负责人字段，与内置模型 'res.users' 建立 m2o 关联，每个课程有一个负责人
    responsible_id = fields.Many2one('res.users',
        ondelete='set null', string=_("Responsible"), index=True)
    # 建立 o2m 关联，一个课程可能存在多个授课，是一个虚拟的关联，是Many2one的逆，course_id为关联字段
    session_ids = fields.One2many('openacademy.session', 'course_id', string="Sessions")

    _sql_constraints = [
        ('name_description_check',
         'CHECK(name != description)',
         "The title of the course should not be the description"),

        ('name_unique',
         'UNIQUE(name)',
         "The course title must be unique"),
    ]

    # 因为为课程名称添加了唯一性约束，所以不能再使用"复制"功能(表单->复制)。重写"复制"方法,允许复制课程对象
    @api.multi
    def copy(self, default=None):
        default = dict(default or {})
        # 根据值模式匹配f name ，模式中的下划线_表示(匹配)任何单个字符;%符号匹配任何包含0个或多个字符的字符串。
        # 多次复制相同文件时 修改序号 (1)
        copied_count = self.search_count([('name', '=like', u"Copy of {}%".format(self.name))])
        if not copied_count:
            new_name = u"Copy of {}".format(self.name)
        else:
            new_name = u"Copy of {} ({})".format(self.name, copied_count)

        default['name'] = new_name
        return super(Course, self).copy(default)
