# -*- coding: utf-8 -*-
{
    'name': "Open Academy",

    'summary': """
        Manage trainings, odoo 模块 测试项目""",

    'description': """
        用于管理培训的开放学院模块:
            ——培训课程
            ——公开课
            ——与会者登记
    """,

    'author': "ZZH",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Test',
    'version': '12.0.0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'board'],

    # always loaded
    'data': [
        'security/openacademy_groups.xml',
        'security/course_security.xml',
        'security/ir.model.access.csv',
        'views/course_views.xml',
        'views/session_views.xml',
        'views/res_partner_views.xml',
        'wizard/create_attendees_views.xml',
        'views/session_board.xml',
        'report/openacademy_session_reports.xml',
        'report/openacademy_session_templates.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}